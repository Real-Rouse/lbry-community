# LBRY Community

These are the theme files of https://lbry.community. 

Info:
1. The theme is Divi with a Child-theme.
2. Do changes in the Child-theme, so that we do not destroy the mother-theme.

To run:
1. Install Wordpress on your localhost or domain with PHP and MySQL.
2. Simply add these files into $WORDPRESS-DIR/wp-content/themes